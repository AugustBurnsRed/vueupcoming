import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import store from './store'
import App from './App'
import _ from 'lodash'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(Vuex)
Vue.http.options.root = 'http://localhost'

import routes from './routes'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  scrollBehavior: () => ({ y: 0 }),
  routes
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  ...App
}).$mount('#app')