import * as types from '../mutation-types'

const state = {
  medias: []
}

const mutations = {
  [types.MEDIA_ADD] (state, media) {
    console.log(media)
  	if (!state.medias.length) {
  		state.medias.unshift(media)
  	}else if (!media){
  		state.medias = []
  	}else{
  		state.medias[0].unshift(media)
  	}
  }
}

export default {
  state,
  mutations
}