import * as types from '../mutation-types'

const state = {
  gameList: '',
  myGames: []
}

const mutations = {
  [types.GAME_SEARCH] (state, gameList) {
    state.gameList = gameList
  },
  [types.GAME_ADD] (state, games) {
  	if (!state.myGames.length) {
  		state.myGames.unshift(games)
  	}else if (!games){
  		state.myGames = []
  	}else{
  		state.myGames[0].unshift(games)
  	}
  }
}

export default {
  state,
  mutations
}