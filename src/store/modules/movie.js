import * as types from '../mutation-types'

const state = {
  movieList: '',
  myMovies: []
}

const mutations = {
  [types.MOVIE_SEARCH] (state, movieList) {
    state.movieList = movieList.results
  },
  [types.MOVIE_ADD] (state, movies) {
  	if (!state.myMovies.length) {
  		state.myMovies.unshift(movies)
  	}else if (!movies){
  		state.myMovies = []
  	}else{
  		state.myMovies[0].unshift(movies)
  	}
  }
}

export default {
  state,
  mutations
}