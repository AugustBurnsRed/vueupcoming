// game
export const medias = state => state.media.medias[0]

// game
export const gameList = state => state.game.gameList
export const myGames = state => state.game.myGames[0]

// movie
export const movieList = state => state.movie.movieList
export const myMovies = state => state.movie.myMovies[0]