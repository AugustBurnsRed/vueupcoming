// all
export const MEDIA_ADD = 'MEDIA_ADD'

// game
export const GAME_SEARCH = 'GAME_SEARCH'
export const GAME_ADD = 'GAME_ADD'

// movie
export const MOVIE_SEARCH = 'MOVIE_SEARCH'
export const MOVIE_ADD = 'MOVIE_ADD'

