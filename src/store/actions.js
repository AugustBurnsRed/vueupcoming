import * as types from './mutation-types'
import Vue from 'vue'
import config from '../js/config'

// movie
export const movieSearch = ({ commit }, name) => {
	Vue.http.get(config.api.music.url+'search/movie?api_key='+config.api.music.key+'&query='+name).then((response) => {
    commit(types.MOVIE_SEARCH, response.data)
  }, (response) => {
    console.log(response)
  })  
}

export const movieAdd = ({ commit }, movie) => {
	Vue.resource('media').save( movie).then((response) => {
    console.log(response.body)
    // reset movie suggestion list
    movieResetSuggestion({commit})
    // reset movie list
    commit(types.MEDIA_ADD, null)
    // fetch all list
    mediaFetch({commit})
  }, (response) => {
    console.log(response)
  })
}

export const movieFetch = ({ commit }) => {
  Vue.http.get('movie').then(function (response) {
    commit(types.MOVIE_ADD, response.body)
  }, function (error) {
    console.log(error)
  })
}

export const movieResetSuggestion = ({ commit }) => {
  commit(types.MOVIE_SEARCH, '')
}

//game
export const gameSearch = ({ commit }, name) => {
  Vue.http.get(config.api.game.url+'?fields=name,first_release_date,cover.cloudinary_id&limit=10&offset=0&order=release_dates.date%3Adesc&search='+name,
    {
      'headers': {
        'X-Mashape-Key': config.api.game.key,
        'Accept': 'application/json'
    }
    }).then((response) => {
    commit(types.GAME_SEARCH, response.data)
  }, (response) => {
    console.log(response)
  })  
}

export const gameAdd = ({ commit }, game) => {
  console.log(game)
  Vue.resource('media').save(game).then((response) => {
    console.log(response.body)
    // reset movie suggestion list
    gameResetSuggestion({commit})
    // reset movie list
    commit(types.MEDIA_ADD, null)
    // fetch all list
    mediaFetch({commit})
  }, (response) => {
    console.log(response)
  })
}

export const gameResetSuggestion = ({ commit }) => {
  commit(types.GAME_SEARCH, '')
}

// all media

// fetch the medias to display them in the list
export const mediaFetch = ({ commit }) => {
  Vue.http.get('media').then(function (response) {
    commit(types.MEDIA_ADD, response.body)
  }, function (error) {
    console.log(error)
  })
}

// destroy the media from the db
export const mediaDestroy = ({ commit }, id) => {
  console.log(id)
  Vue.http.delete('media/'+id).then((response) => {
    // reset media list
    commit(types.MEDIA_ADD, null)
    // fetch all list
    mediaFetch({commit})
  }, function (error) {
    console.log(error)
  })
}