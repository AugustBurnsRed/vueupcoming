import Movie from './components/Movie/Movie.vue'

export default [
  {
    path: '/',
    component: { template: '<h1>Accueil</h1>' }
  },
  {
    path: '/movie',
    component: Movie
  }
]
